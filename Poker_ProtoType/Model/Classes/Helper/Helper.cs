﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker_ProtoType
{
    public static class Helper
    {
        public static string GetCardPath(Card card)
        {
            string color = card.Color.ToString().ToLower();
            string value = "";
            string path = @"c:\users\royale\documents\visual studio 2015\Projects\Poker_ProtoType\Poker_ProtoType\extern\Cards\";

            if ((int)card.Value > 10)
            {
                if ((int)card.Value < 14)
                    color += "2";

                value = card.Value.ToString().ToLower();
            }
            else
            {
                value = ((int)card.Value).ToString();
            }
            return path + value + "_of_" + color + ".png";
        }
    }
}
