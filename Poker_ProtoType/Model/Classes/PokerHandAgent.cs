﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker_ProtoType
{
    public class PokerHandAgent
    {
        #region ctor
        public PokerHandAgent(IEnumerable<Card> allCards)
        {
            this.CardsAll = allCards.ToList();
            this.CardsHit = new List<Card>();
            
            _groupedCardsByValue = _cardsAll.GroupBy(x => x.Value);
            CheckForHands();
        }
        #endregion

        #region Fields
        private List<Card> _cardsAll;
        private List<Card> _cardsHitted;
        private IEnumerable<IGrouping<CardValue, Card>> _groupedCardsByValue;
        private HandValues _handValue;
        #endregion

        #region Properties
        private List<Card> CardsAll
        {
            get { return _cardsAll; }
            set
            {
                _cardsAll = value;
            }
        }
        private List<Card> CardsHit
        {
            get { return _cardsHitted; }
            set { _cardsHitted = value; }
        }
        private List<Card> CardsNoHit
        {
            get
            {
                return this._cardsAll?.Except(this.CardsHit)?.ToList();
            }
        }
        private IEnumerable<IGrouping<CardValue, Card>> GroupedCardsByValue
        {
            get
            {
                return _groupedCardsByValue;
            }
        }
        
        public HandValues HandValue
        {
            get { return _handValue; }
            set { _handValue = value; }
        }
        public List<Card> GetHand
        {
            get
            {
                return GetFinalHand();
            }
        }
        #endregion

        #region Methods
        private void CheckForHands()
        {
            this.CardsHit = GetHighCards(5);
            this.HandValue = HandValues.HighCard;

            bool pairs = TryGetPairs();
            bool threeOfaKind = TryGetThreeOfAKind();
            bool straight = TryGetStraight();
            bool flush = TryGetFlush();

            if (pairs && threeOfaKind)
                GetFullHouse();

            GetFourOfAKind();

            if (straight && flush)
                GetStraightFulsh();
        }
        private List<Card> GetFinalHand()
        {
            List<Card> cards = new List<Card>();
            cards.AddRange(CardsHit);
            cards.AddRange(GetHighCards(5 - CardsHit.Count));
            return cards;
        }
        private List<Card> GetHighCards(int howMuch) => this.CardsNoHit.OrderByDescending(x => x.Value).Take(howMuch).ToList();

        #region Get Hands
        private bool TryGetPairs()
        {
            var grpPairs = GroupedCardsByValue.Where(x => x.Count() == 2);

            if (grpPairs == null || grpPairs.Count() <= 0)
                return false;

            this.CardsHit.Clear();
            this.HandValue = HandValues.Pair;

            if (grpPairs.Count() > 1)
            {
                grpPairs = grpPairs.OrderByDescending(x => x.Key).Take(2);
                this.HandValue = HandValues.TwoPairs;
            }

            foreach (var group in grpPairs)
                group.ToList().ForEach(x => this.CardsHit.Add(x));

            return true;
        }
        private bool TryGetThreeOfAKind()
        {
            var grpThreeOfAKind = GroupedCardsByValue.Where(x => x.Count() == 3);

            if (grpThreeOfAKind == null || grpThreeOfAKind.Count() <= 0)
                return false;

            this.CardsHit.Clear();
            this.HandValue = HandValues.ThreeOfAKind;

            if (grpThreeOfAKind.Count() == 2)
                grpThreeOfAKind = grpThreeOfAKind.OrderByDescending(x => x.Key).Take(1);

            grpThreeOfAKind.ToList()[0].ToList().ForEach(x => this.CardsHit.Add(x));

            return true;
        }
        private bool GetFourOfAKind()
        {
            var grpFourOfAKind = GroupedCardsByValue.Where(x => x.Count() == 4);

            if (grpFourOfAKind == null || grpFourOfAKind.Count() <= 0)
                return false;

            this.CardsHit.Clear();
            this.HandValue = HandValues.Poker;

            grpFourOfAKind.ToList()[0].ToList().ForEach(x => this.CardsHit.Add(x));

            return true;
        }

        private bool TryGetStraight()
        {
            List<Card> sortedList = this.CardsAll.ToList();

            List<Card> straight = new List<Card>();

            Card ace = sortedList.Where(x => x.Value == CardValue.Ace)?.FirstOrDefault();
            if (ace != null)
                sortedList.Add(new Card(CardValue.One, ace.Color));

            sortedList = sortedList.OrderBy(x => x.Value).ToList();

            for (int counter = 0; counter < sortedList.Count(); counter++)
            {
                if (sortedList.ElementAtOrDefault(counter + 1) == null)
                    break;

                CardValue cardValue = sortedList[counter].Value;

                if (cardValue + 1 == sortedList[counter + 1].Value)
                {
                    if (!straight.Any())
                        straight.Add(sortedList[counter]);

                    straight.Add(sortedList[counter + 1]);
                }
            }

            if (straight.Count >= 5)
            {
                if (ace != null)
                {
                    var aceAsOne = straight.Where(x => x.Value == CardValue.One).FirstOrDefault();
                    if (aceAsOne != null)
                    {
                        straight.Remove(aceAsOne);
                        straight.Add(ace);
                    }
                }

                this.HandValue = HandValues.Straight;
                this.CardsHit = straight.OrderByDescending(x => x.Value).Take(5).ToList().OrderBy(x => x.Value).ToList();

                return true;
            }
            return false;
        }
        private bool TryGetFlush()
        {
            var flush = this.CardsAll.GroupBy(x => x.Color).Where(x => x.Count() >= 5).ToList().FirstOrDefault();

            if (flush == null)
                return false;

            this.CardsHit.Clear();
            this.HandValue = HandValues.Flush;

            flush.ToList().ForEach(x => this.CardsHit.Add(x));
            this.CardsHit = this.CardsHit.OrderByDescending(x => x.Value).Take(5).ToList();

            return true;
        }
        private bool GetFullHouse()
        {
            var grpThreeOfAKind = GroupedCardsByValue.Where(x => x.Count() == 3).OrderByDescending(x => x.Key).FirstOrDefault();
            var grpPairs = GroupedCardsByValue.Where(x => x.Count() == 2).OrderByDescending(x => x.Key).FirstOrDefault();

            if (grpThreeOfAKind != null && grpPairs != null)
            {
                this.CardsHit.Clear();
                this.HandValue = HandValues.FullHouse;

                grpThreeOfAKind.ToList().ForEach(x => this.CardsHit.Add(x));
                grpPairs.ToList().ForEach(x => this.CardsHit.Add(x));

                return true;
            }

            return false;
        }

        private bool GetStraightFulsh()
        {
            TryGetFlush();
            CardValue maxValFlush = this.CardsHit.FirstOrDefault().Value;

            TryGetStraight();
            CardValue maxValStraight = this.CardsHit.FirstOrDefault().Value;

            if (maxValStraight == CardValue.None || maxValFlush == CardValue.None)
                return false;

            if (maxValStraight == maxValFlush)
            {
                this.HandValue = HandValues.StraightFlush;

                if (maxValFlush == CardValue.Ace)
                    this.HandValue = HandValues.RoyalFlush;

                return true;
            }
            return false;
        }
        #endregion

        #endregion
    }

    public enum HandValues
    {
        None = -1,
        HighCard = 1,
        Pair = 2,
        TwoPairs = 3,
        ThreeOfAKind = 4,
        Straight = 5,
        Flush = 6,
        FullHouse = 7,
        Poker = 8,
        StraightFlush = 9,
        RoyalFlush = 10
    }
}
