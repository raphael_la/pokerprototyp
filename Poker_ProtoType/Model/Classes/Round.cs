﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Poker_ProtoType
{
    public class Round : INotifyPropertyChanged
    {
        #region ctor
        public Round(IEnumerable<Player> player, int smallBlind, int bigBlind, int roundCount)
        {
            this.BigBlindRotationCounter = 0;
            this._roundCount = roundCount;

            this.Players = player.ToList();
            this.Players.ForEach(x => x.OnPlayerSetAction += OnPlayerSetAction);

            this.SmallBlind = smallBlind;
            this.BigBlind = bigBlind;

            this.Pot = new Pot(roundCount);
            this.GivenCards = new List<Card>();

            this._commonCards = new ObservableCollection<Card>();
            this._commonCards.CollectionChanged += CommonCards_CollectionChanged;

            foreach (Player p in this.Players)
            {
                p.Cards = new List<Card>();

                p.SetMoney = 0;
                p.RotationCount = 0;
                p.Active = false;
            }
        }
        #endregion

        #region Fields
        private bool _riverIsShowen = false;
        private bool _firstRotation = true;
        private int raiserID;
        private int _bigBlindRotationCounter = 0;
        private int _roundCount;

        private int _smallBlind;
        private int _bigBlind;

        private List<Player> _players;
        private ObservableCollection<Card> _commonCards;
        
        private int _palayerCount;
        private List<Card> _givenCards;
        private Pot _pot;
        #endregion

        #region Properties

        #region Player
        public List<Player> Players
        {
            get
            {
                return _players;
            }

            set
            {
                _players = value;
                this.PlayerCount = _players.Count;
            }
        }

        public Player DealerPlayer
        {
            get
            {
                return this.Players.Where(x => x.Id == 1)?.FirstOrDefault();
            }
        }
        public Player SmallBlindPlayer
        {
            get
            {
                return this.Players.Where(x => x.IsSmallBlind == true)?.FirstOrDefault();
            }
        }
        public Player BigBlindPlayer
        {
            get
            {
                return this.Players.Where(x => x.IsBigBlind)?.FirstOrDefault();
            }
        }

        private Player CurrentActivatedPlayer
        {
            get
            {
                return this.Players.Where(x => x.Active)?.FirstOrDefault();
            }
        }
        private Player NextPlayer
        {
            get
            {
                int curId = this.CurrentActivatedPlayer.Id;
                if (curId == this.PlayerCount)
                    return this.Players.Where(x => x.Id == 1).FirstOrDefault();

                var yy = ++curId;

                return this.Players.Where(x => x.Id == yy).FirstOrDefault();
            }
        }

        public int PlayerCount
        {
            get
            {
                return _palayerCount;
            }

            set
            {
                _palayerCount = value;
            }
        }
        #endregion

        #region Cards
        public ObservableCollection<Card> CommonCards
        {
            get
            {
                return _commonCards;
            }
        }
        public List<Card> Flop
        {
            get
            {
                return CommonCards.Where(x => x.Typ == CardTyp.Flop)?.ToList();
            }
        }
        public Card Turn
        {
            get
            {
                return CommonCards.Where(x => x.Typ == CardTyp.Turn)?.FirstOrDefault();
            }

        }
        public Card River
        {
            get
            {
                return CommonCards.Where(x => x.Typ == CardTyp.River)?.FirstOrDefault();
            }
        }

        public List<Card> GivenCards
        {
            get
            {
                return _givenCards;
            }
            private set
            {
                _givenCards = value;
            }
        }
        #endregion

        #region Money
        public Pot Pot
        {
            get
            {
                return _pot;
            }
            set
            {
                _pot = value;
            }
        }

        public int SmallBlind
        {
            get
            {
                return _smallBlind;
            }
            set
            {
                _smallBlind = value;
            }
        }
        public int BigBlind
        {
            get
            {
                return _bigBlind;
            }
            set
            {
                _bigBlind = value;
            }
        }
        #endregion

        #region Functioncal
        public int BigBlindRotationCounter
        {
            get
            {
                return _bigBlindRotationCounter;
            }

            set
            {
                _bigBlindRotationCounter = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #endregion

        #region Events

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var propChanged = this.PropertyChanged;
            propChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion


        public delegate void FinishedRound(EventArgs e);
        private event FinishedRound _onFinishedRound;
        public event FinishedRound OnFinishedRound
        {
            add
            {
                if(_onFinishedRound == null)
                {
                    _onFinishedRound -= value;
                    _onFinishedRound += value;
                }
            }
            remove
            {
                _onFinishedRound -= value;
            }
        }

        private void OnPlayerSetAction(PlayerEventArgs e)
        {
            switch (e.Player.PlayerAction)
            {
                case PlayerAction.Check:
                    this.PlayerCheck(e.Player);
                    break;

                case PlayerAction.Fold:
                    this.PlayerFold(e.Player);
                    break;

                case PlayerAction.Call:
                    this.PlayerCall(e.Player);
                    break;

                case PlayerAction.Raise:
                    this.PlayerRaise(e.Player);
                    break;

                case PlayerAction.MissedTime:
                    this.PlayerMissedTime(e.Player);
                    break;
            }

            this.ActivatePlayer();
        }
        private void CommonCards_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            NotifyPropertyChanged(nameof(Round.CommonCards));
        }

        #endregion

        #region Methods

        #region Give Cards
        public void GiveCards()
        {
            //Give them Players Bananas
            foreach (Player player in this.Players)
            {
                player.Cards.Clear();

                for (int i = 0; i < Game.maxCards; i++)
                    player.AddCard(GiveCard(CardTyp.Player));
            }

            //Give Flop
            for (int i = 0; i < 3; i++)
                CommonCards.Add(GiveCard(CardTyp.Flop));

            //Give Turn and River
            CommonCards.Add(GiveCard(CardTyp.Turn));
            CommonCards.Add(GiveCard(CardTyp.River));
        }
        private Card GiveCard(CardTyp typ)
        {
            //Random to get value and color of Card
            Random random = new Random(DateTime.Now.Millisecond);
            Card newCard = null;

            bool card = false;
            while (!card)
            {
                CardValue cardValue = (CardValue)random.Next(2, 15);
                CardColor cardColor = (CardColor)random.Next(1, 5);

                newCard = new Card(cardValue, cardColor, typ);

                if (!this.GivenCards.Any(x => x.Value == newCard.Value && x.Color == newCard.Color))
                    card = true;
            }
            this.GivenCards.Add(newCard);
            return newCard;
        }
        #endregion

        #region Set Blinds
        public void SetBlinds()
        {
            DoSmallBlind();
            DoBigBlind();
        }
        private void DoBigBlind()
        {
            BigBlindPlayer.Raise(this.BigBlind);
        }
        private void DoSmallBlind()
        {
            //ToDo evtl in Logik implementieren
            this.SmallBlindPlayer.Active = true;
            this.SmallBlindPlayer.SetMoney += this.SmallBlind;
            this.SmallBlindPlayer.Stack -= this.SmallBlind;

            this.Pot.ToCall += this.SmallBlind;
            this.Pot.Money += this.SmallBlind;

            this.ActivatePlayer(this.BigBlindPlayer);
        }
        #endregion

        #region Player Actions
        private void PlayerRaise(Player player)
        {
            player.Stack -= player.SetMoney;

            this.Pot.Money += player.SetMoney;
            this.Pot.ToCall = player.SetMoney;         

            this.raiserID = player.Id;
        }
        private void PlayerCall(Player player)
        {
            int calc = Pot.ToCall - player.SetMoney;
            player.SetMoney += calc;
            player.Stack -= calc;
            this.Pot.Money += calc;
        }
        private void PlayerFold(Player player)
        {
            //ToDo Programmieren
            //this.Players.Remove(player);
        }
        private void PlayerCheck(Player player)
        {
        }
        private void PlayerMissedTime(Player player)
        {
            if (this.Pot.ToCall > 0)
                this.PlayerFold(player);
            else
                this.PlayerCheck(player);
        }
        #endregion
        
        #region Show Common Cards
        public void ShowFlop() => this.Flop.ToList().ForEach(x => x.IsShown = true);
        public void ShowTurn() => this.Turn.IsShown = true;
        public void ShowRiver() => this.River.IsShown = true;

        public void HideAll()
        {
            this.GivenCards.ToList().ForEach(x => x.IsShown = false);
        }

        private void ShowNextCard()
        {
            if (!this.Flop[0].IsShown)
                this.ShowFlop();
            else if (this.Flop[0].IsShown && this.Turn.IsShown)
            {
                this.ShowRiver();
                _riverIsShowen = true;
            }
            else
                this.ShowTurn();
        }
        #endregion


        #region !!UNMANAGED!!
        public void ActivatePlayer(Player nextPlayer = null)
        {
            //ToDo Fuck that alg.

            if (nextPlayer == null)
                nextPlayer = this.NextPlayer;

            var currPlayer = this.CurrentActivatedPlayer;
            
            bool moneyIsEven = currPlayer.SetMoney == nextPlayer.SetMoney;

            bool ok = false;

            //bug
            if (currPlayer.PlayerAction == PlayerAction.Check && nextPlayer.PlayerAction == PlayerAction.Check && currPlayer.RotationCount == nextPlayer.RotationCount)
                ok = true;
            
            else if (currPlayer.PlayerAction == PlayerAction.Raise)
                ok = false;
            
            else if(currPlayer.PlayerAction == PlayerAction.Call && moneyIsEven && nextPlayer.Id == raiserID && !_firstRotation)
                ok = true;
            else if(currPlayer.IsBigBlind && currPlayer.PlayerAction == PlayerAction.Check && _firstRotation)
                ok = true;
            
            if (ok)
            {
                if (this._riverIsShowen)
                {
                    this.FinishThatRound();
                    return;
                }

                this.ShowNextCard();
                
                this.Pot.ToCall = 0;
                this.Players.ForEach(x => x.SetMoney = 0);
                this.Players.ForEach(x => x.RotationCount = 0);

                SetPlayerActive(this.SmallBlindPlayer);
                SmallBlindPlayer.AllowedToCheck = true;
                SmallBlindPlayer.AllowedToRaise = true;
                SmallBlindPlayer.AllowedToCall = false;

                _firstRotation = false;

                return;
            }

            if (currPlayer.PlayerAction == PlayerAction.Raise || (currPlayer.PlayerAction == PlayerAction.Call && !ok))
            {
                this.NextPlayer.AllowedToCall = true;
                this.NextPlayer.AllowedToRaise = true;
                this.NextPlayer.AllowedToCheck = false;
            }
            if (currPlayer.PlayerAction == PlayerAction.Check)
            {
                this.NextPlayer.AllowedToCheck = true;
                this.NextPlayer.AllowedToCall = false;
                this.NextPlayer.AllowedToRaise = true;
            }

            if (_firstRotation && nextPlayer.IsBigBlind && moneyIsEven && nextPlayer.RotationCount > 0)
            {
                this.NextPlayer.AllowedToCheck = true;
                this.NextPlayer.AllowedToCall = false;
            }

            if (this.CurrentActivatedPlayer.IsBigBlind && _firstRotation)
            {
                if (BigBlindRotationCounter == 1)
                    this._firstRotation = false;

                this.BigBlindRotationCounter++;
            }

            SetPlayerActive(nextPlayer);
        }

        //ToDo implementieren
        private void SetNextPlayerAlowness()
        {
            //if (playerRaised || (playerCalled && !playerAreEven))
            //{
            //    this.NextPlayer.AllowedToCall = true;
            //    this.NextPlayer.AllowedToRaise = true;
            //    this.NextPlayer.AllowedToCheck = false;
            //}
            //if (playerChecked)
            //{
            //    this.NextPlayer.AllowedToCheck = true;
            //    this.NextPlayer.AllowedToCall = false;
            //    this.NextPlayer.AllowedToRaise = true;
            //}

            //if (_firstRotation && this.NextPlayer.IsBigBlind && setMoneyIsEven && this.NextPlayer.RotationCount > 0)
            //{
            //    this.NextPlayer.AllowedToCheck = true;
            //    this.NextPlayer.AllowedToCall = false;
            //}
        }

        private void SetPlayerActive(Player player)
        {
            player.Active = true;
            this.Players.Where(x => x != player).ToList().ForEach(x => x.Active = false);
        }
        private void FinishThatRound()
        {
            var winner = this.GetWinner();
            
            ClearRound();
            _onFinishedRound?.Invoke(new EventArgs());
        }
        private PlayerHand GetWinner()
        {
            List<PlayerHand> playerHands = new List<PlayerHand>();

            List<Card> commonCards = this.GivenCards.Where(x => x.Typ != CardTyp.Player).ToList();

            foreach (Player p in this.Players)
            {
                List<Card> allCards = commonCards.ToList();
                p.Cards.ForEach(x => allCards.Add(x));

                PokerHandAgent agent = new PokerHandAgent(allCards);
                playerHands.Add(new PlayerHand(p, agent.GetHand, agent.HandValue));
            }

            return playerHands.OrderByDescending(x => x.HandValue).FirstOrDefault();
        }
        public void ClearRound()
        {
            this.Players.ForEach(x => x.OnPlayerSetAction -= OnPlayerSetAction);
            this._commonCards.CollectionChanged -= CommonCards_CollectionChanged;
        }
        #endregion



        #endregion


    }

    public class PlayerHand
    {
        public PlayerHand() { }
        public PlayerHand(Player player, List<Card> hand, HandValues handValue) : this()
        {
            this.Player = player;
            this.Hand = hand;
            this.HandValue = handValue;
        }

        private Player _player;
        private List<Card> _hand;
        private HandValues _handValue;

        public Player Player
        {
            get
            {
                return _player;
            }

            set
            {
                _player = value;
            }
        }
        public List<Card> Hand
        {
            get
            {
                return _hand;
            }

            set
            {
                _hand = value;
            }
        }
        public HandValues HandValue
        {
            get
            {
                return _handValue;
            }

            set
            {
                _handValue = value;
            }
        }
    }
}
