﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Poker_ProtoType
{
    public class Pot : INotifyPropertyChanged
    {
        #region ctor
        public Pot(int potCount)
        {
            this._potCount = potCount;
            this._money = 0;
        }
        #endregion

        #region Fields
        private int _potCount;

        private int _money;
        private int _toCall;
        #endregion

        #region Properties
        public int Money
        {
            get
            {
                return _money;
            }

            set
            {
                _money = value;
                NotifyPropertyChanged();
            }
        }
        public int ToCall
        {
            get
            {
                return _toCall;
            }

            set
            {
                _toCall = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region Events

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        #endregion

        #endregion
    }
}
