﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Timers;

namespace Poker_ProtoType
{
    public class Player : INotifyPropertyChanged
    {
        #region ctor
        public Player(string name)
        {
            this.Cards = new List<Card>();
            this.Name = name;
            this.Stack = 0;
            this.Id = 0;
            this.SetMoney = 0;
            this.RotationCount = 0;
            this.PlayerAction = PlayerAction.None;

            this.maxCards = 2;

            InitTimer();
        }
        private void InitTimer()
        {
            this.Timer = new TimerPlus();
            this.Timer.Enabled = false;
            this.Timer.Interval = 5000;
            this.Timer.AutoReset = true;
            this.Timer.Elapsed += this.PlayerTimer_Elapsed;
        }
        #endregion

        #region Fields
        private int maxCards;
        public TimerPlus Timer;

        private int _id;
        private string _name;

        private int _stack;
        private int _setMoney;

        private List<Card> _cards;
        private PlayerAction _playerAction;

        private bool _allowedToCheck = false;
        private bool _allowedToRaise = false;
        private bool _allowedToCall = false;
        private bool _allowedToFold = false;

        private bool _active = false;

        private bool _bigBlind = false;
        private bool _smallBlind = false;

        private int _rotationCount;
        #endregion

        #region Properties        
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                NotifyPropertyChanged();
            }
        }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                NotifyPropertyChanged();
            }
        }

        public int Stack
        {
            get
            {
                return _stack;
            }
            set
            {
                _stack = value;
                NotifyPropertyChanged();
            }
        }
        public int SetMoney
        {
            get
            {
                return _setMoney;
            }
            set
            {
                _setMoney = value;
                NotifyPropertyChanged();
            }
        }

        public List<Card> Cards
        {
            get
            {
                return _cards;
            }
            set
            {
                _cards = value;
                NotifyPropertyChanged();
            }
        }
        public PlayerAction PlayerAction
        {
            get
            {
                return _playerAction;
            }
            set
            {
                _playerAction = value;
                if (_playerAction != PlayerAction.None)
                {
                    _onPlayerSetAction?.Invoke(new PlayerEventArgs(this));
                    NotifyPropertyChanged();
                    this.GoToSleepPlayer();
                }
            }
        }

        public bool AllowedToCheck
        {
            get
            {
                return _allowedToCheck;
            }
            set
            {
                _allowedToCheck = value;
                NotifyPropertyChanged();
            }
        }
        public bool AllowedToRaise
        {
            get
            {
                return _allowedToRaise;
            }

            set
            {
                _allowedToRaise = value;
                NotifyPropertyChanged();
            }
        }
        public bool AllowedToFold
        {
            get
            {
                return _allowedToFold;
            }

            set
            {
                _allowedToFold = value;
                NotifyPropertyChanged();
                if (_allowedToFold)
                {
                    return;
                }

            }

        }
        public bool AllowedToCall
        {
            get
            {
                return _allowedToCall;
            }

            set
            {
                _allowedToCall = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsBigBlind
        {
            get
            {
                return _bigBlind;
            }
            set
            {
                _bigBlind = value;
                NotifyPropertyChanged();
            }
        }
        public bool IsSmallBlind
        {
            get
            {
                return _smallBlind;
            }
            set
            {
                _smallBlind = value;
                NotifyPropertyChanged();
            }
        }

        public bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
                if (_active) WakeUpPlayer();
                NotifyPropertyChanged();
            }
        }

        public int RotationCount
        {
            get
            {
                return _rotationCount;
            }

            set
            {
                _rotationCount = value;
                NotifyPropertyChanged();
            }
        }
        #endregion

        #region Events

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        #endregion

        public delegate void PlayerDidAction(PlayerEventArgs e);
        private event PlayerDidAction _onPlayerSetAction;
        public event PlayerDidAction OnPlayerSetAction
        {
            add
            {
                if(_onPlayerSetAction == null)
                {
                    _onPlayerSetAction -= value;
                    _onPlayerSetAction += value;
                }
            }
            remove
            {
                _onPlayerSetAction -= value;
            }
        }
        
        public delegate void CardAdded(PlayerEventArgs e);
        private event CardAdded _onCardAdded;
        public event CardAdded OnCardAdded
        {
            add
            {
                if(_onCardAdded == null)
                {
                    _onCardAdded -= value;
                    _onCardAdded += value;
                }
            }
            remove
            {
                _onCardAdded -= value;
            }
        }

        private void PlayerTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //if (this.PlayerAction == PlayerAction.None)
            //    this.PlayerAction = PlayerAction.MissedTime;
        }
        #endregion

        #region Methods
        internal void AddCard(CardValue value, CardColor color)
        {
            Cards.Add(new Card(value, color));
        }
        internal void AddCard(Card card)
        {
            if (this.Cards.Count() == this.maxCards)
                return;

            Cards.Add(card);
            _onCardAdded?.Invoke(new PlayerEventArgs(this));
        }

        public void Fold()
        {
            this.PlayerAction = PlayerAction.Fold;
        }
        public void Raise(int money)
        {
            this.SetMoney = money;
            this.PlayerAction = PlayerAction.Raise;
        }
        public void Check()
        {
            this.PlayerAction = PlayerAction.Check;
        }
        public void Call()
        {
            this.PlayerAction = PlayerAction.Call;
        }

        public void WakeUpPlayer()
        {
            this.PlayerAction = PlayerAction.None;
            this.AllowedToFold = true;
            this.Timer.Enabled = true;
        }
        private void GoToSleepPlayer()
        {
            this.Timer.Enabled = false;

            this.AllowedToFold = false;
            this.AllowedToCall = false;
            this.AllowedToRaise = false;
            this.AllowedToCheck = false;

            this.RotationCount++;
        }
        #endregion

    }
    public enum PlayerAction
    {
        None = -1,
        Check = 0,
        Fold = 1,
        Raise = 2,
        Call = 3,
        MissedTime = 4,
        Activated = 5
    }
    public class PlayerEventArgs : EventArgs
    {
        public PlayerEventArgs(Player player)
        {
            this.Player = player;
        }

        private Player _player;
        public Player Player
        {
            get
            {
                return _player;
            }

            set
            {
                _player = value;
            }
        }
    }
}
