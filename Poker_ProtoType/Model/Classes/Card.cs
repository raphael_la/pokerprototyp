﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Timers;

namespace Poker_ProtoType
{
    public class Card : INotifyPropertyChanged
    {
        #region ctor
        public Card() { this.IsShown = false; }
        public Card(CardValue value, CardColor color) : this()
        {
            this.Value = value;
            this.Color = color;
        }
        public Card(CardValue value, CardColor color, CardTyp typ) : this(value, color)
        {
            this.Typ = typ;
        }
        #endregion

        #region Events

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        #endregion

        public delegate void CardEvent(CardEventArgs e);
        private event CardEvent _onCardIsShown;
        public event CardEvent OnCardIsShown
        {
            add
            {
                if (_onCardIsShown == null)
                {
                    _onCardIsShown -= value;
                    _onCardIsShown += value;
                }
            }
            remove
            {
                _onCardIsShown -= value;
            }
        }

        #endregion

        #region Fields
        private CardValue _value;
        private CardColor _color;
        private CardTyp _typ;
        private bool _isShown;
        #endregion

        #region Properties
        public CardValue Value
        {
            get
            {
                return _value;
            }

            set
            {
                _value = value;
            }
        }
        public CardColor Color
        {
            get
            {
                return _color;
            }

            set
            {
                _color = value;
            }
        }
        public CardTyp Typ
        {
            get
            {
                return _typ;
            }

            set
            {
                _typ = value;
                NotifyPropertyChanged();
            }
        }
        public bool IsShown
        {
            get
            {
                return _isShown;
            }

            set
            {
                _isShown = value;
                NotifyPropertyChanged();
                _onCardIsShown?.Invoke(new CardEventArgs(this));
            }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return this.Value + "\r\n" + this.Color;
        }
        #endregion
    }

    public enum CardValue
    {
        None = -1,
        One = 1,
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8,
        Nine = 9,
        Ten = 10,
        Jack = 11,
        Queen = 12,
        King = 13,
        Ace = 14
    }
    public enum CardColor
    {
        None = -1,
        Hearts = 1,
        Spades = 2,
        Diamonds = 3,
        Clubs = 4
    }
    public enum CardTyp
    {
        None = -1,
        Flop = 1,
        Turn = 2,
        River = 3,
        Player = 4
    }

    public class CardEventArgs : EventArgs
    {
        public CardEventArgs(Card card)
        {
            this.Card = card;
        }
        private Card _card;
        public Card Card
        {
            get
            {
                return _card;
            }

            set
            {
                _card = value;
            }
        }
    }
}

