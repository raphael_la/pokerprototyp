﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker_ProtoType
{
    public class Game
    {
        #region ctor
        public Game()
        {
            this.Players = new List<Player>();
            this.GameIsStarted = false;
            this.BigBlind = 20;
            this.SmallBlind = 10;
            this.roundCount = 1;
        }
        #endregion

        #region Fields
        //ToDo an Round im Ctor übergeben
        public readonly static int maxCards = 2;
        private readonly int startMoney = 5000;
        private int roundCount = 1;
        
        private List<Player> _players;

        private bool _gameIsStarted;
        private int _smallBlind;
        private int _bigBlind;

        private Player _playerBigBlind;
        private Player _playerSmallBlind;

        private Round _currentRound;
        #endregion

        #region Properties
        public List<Player> Players
        {
            get
            {
                return _players;
            }

            set
            {
                _players = value;
            }
        }
        
        public bool GameIsStarted
        {
            get
            {
                return
                    (_gameIsStarted);
            }
            set
            {
                _gameIsStarted = value;
            }
        }

        public int SmallBlind
        {
            get
            {
                return _smallBlind;
            }
            set
            {
                _smallBlind = value;
            }
        }
        public int BigBlind
        {
            get
            {
                return _bigBlind;
            }

            set
            {
                _bigBlind = value;
            }
        }

        public Player PlayerBigBlind
        {
            get
            {
                return _playerBigBlind;
            }

            set
            {
                _playerBigBlind = value;
                if (_playerBigBlind != null)
                {
                    _playerBigBlind.IsBigBlind = true;
                    this.Players.Where(p => p != _playerBigBlind).ToList().ForEach(p => p.IsBigBlind = false);
                }

            }
        }
        public Player PlayerSmallBlind
        {
            get
            {
                return _playerSmallBlind;
            }
            set
            {
                _playerSmallBlind = value;
                if (_playerSmallBlind != null)
                {
                    _playerSmallBlind.IsSmallBlind = true;
                    this.Players.Where(p => p != _playerSmallBlind).ToList().ForEach(p => p.IsSmallBlind = false);
                }
            }
        }

        public Round CurrentRound
        {
            get
            {
                return _currentRound;
            }
            set
            {
                _currentRound = value;
            }
        }
        #endregion

        #region Events
        public delegate void NewRoundSet(GameEventArgs e);
        private event NewRoundSet _onNewRoundSet;
        public event NewRoundSet OnNewRoundSet
        {
            add
            {
                _onNewRoundSet -= value;
                _onNewRoundSet += value;
            }
            remove
            {
                _onNewRoundSet -= value;
            }
        }

        private void CurrentRound_OnFinishedRound(EventArgs e)
        {
            throw new Exception();

            //ToDo Bug
            //this.StartNewRound();
        }
        #endregion

        #region Methods

        #region Add Player
        public void AddPlayer(Player player)
        {
            if (!this.Players.Contains(player) && !GameIsStarted)
                this.Players.Add(player);
        }
        public void AddPlayer(IEnumerable<Player> player) => player.ToList().ForEach(x => this.AddPlayer(x));
        #endregion

        public void StartGame()
        {
            if (this.Players.Count == 0)
                return;

            this.GameIsStarted = true;
            this.Players.ForEach(x => x.Stack = startMoney);
        }
        public void StartNewRound()
        {
            if (!this.GameIsStarted)
                return;

            if (this.CurrentRound != null)
                this.CurrentRound.ClearRound();
            
            SetPlayerIDs(roundCount == 1);
            SetBlindRoles();
            
            this.CurrentRound = new Round(this.Players, this.SmallBlind, this.BigBlind, roundCount);
            this.roundCount++;

            this.CurrentRound.OnFinishedRound += CurrentRound_OnFinishedRound;
            _onNewRoundSet?.Invoke(new GameEventArgs(this));
        }

        private void SetPlayerIDs(bool firstTime = false)
        {
            if (firstTime)
            {
                for (int i = 0; i < this.Players.Count; i++)
                    this.Players[i].Id = i + 1;
            }
            else
            {
                var cnt = this.Players.Count;

                for (int i = 0; i < cnt; i++)
                {
                    if (this.Players[i].Id == 1)
                        this.Players[i].Id = this.Players.Count;
                    else
                        this.Players[i].Id--;
                }
            }
        }
        private void SetBlindRoles()
        {
            if (this.Players.Count() > 2)
            {
                this.PlayerBigBlind = this.Players.Where(x => x.Id == 3).FirstOrDefault();
                this.PlayerSmallBlind = this.Players.Where(x => x.Id == 2).FirstOrDefault();
            }
            else
            {
                this.PlayerBigBlind = this.Players.Where(x => x.Id == 2).FirstOrDefault();
                this.PlayerSmallBlind = this.Players.Where(x => x.Id == 1).FirstOrDefault();
            }
        }
        #endregion
    }
    
    public class GameEventArgs : EventArgs
    {
        public GameEventArgs(Game game)
        {
            this.Game = game;
        }

        private Game _game;
        public Game Game
        {
            get
            {
                return _game;
            }

            set
            {
                _game = value;
            }
        }
    }
}
