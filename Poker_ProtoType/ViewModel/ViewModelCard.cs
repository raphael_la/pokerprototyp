﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker_ProtoType.ViewModel
{
    public class ViewModelCard : ObservableObject
    {
        public ViewModelCard(string path)
        {
            this.Path = path;
        }

        private string _path;
        public string Path
        {
            get
            {
                return _path;
            }

            set
            {
                _path = value;
            }
        }
    }
}
