﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Poker_ProtoType
{
    public class ViewModelMain : ObservableObject
    {
        public ViewModelMain()
        {
            _player1 = new Player("");
            _player2 = new Player("");
            _player3 = new Player("");
            _player4 = new Player("");
            List<Player> players = new List<Player>()
                {
                    _player1,
                    _player2,
                    _player3,
                    _player4
                };

            _game = new Game();
            _game.OnNewRoundSet += _game_OnNewRoundSet;
            _game.AddPlayer(players);

            players.ForEach(x => this.ViewModelPlayer.Add(new ViewModelPlayer(x)));
            
            _player1.Name = "Player 1";
            _player2.Name = "Player 2";
            _player3.Name = "Player 3";
            _player4.Name = "Player 4";
            _game.StartGame();
            NewRound();
        }

        private void _game_OnNewRoundSet(GameEventArgs e)
        {
            this._game.CurrentRound.PropertyChanged += CurrentRound_PropertyChanged;
            this._game.CurrentRound.Pot.PropertyChanged += Pot_PropertyChanged;
            foreach (var c in _game.CurrentRound.GivenCards)
                c.OnCardIsShown += Card_OnCardIsShowen;
            
        }

        Player _player1;
        Player _player2;
        Player _player3;
        Player _player4;
        Game _game;

        List<ViewModelPlayer> _viewModelPlayer = new List<ViewModelPlayer>();
        public List<ViewModelPlayer> ViewModelPlayer
        {
            get
            {
                return _viewModelPlayer;
            }

            set
            {
                _viewModelPlayer = value;
            }
        }

        private void CurrentRound_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case (nameof(Round.Flop)):
                    this.SetFlop();
                    break;
                case (nameof(Round.Turn)):
                    this.SetTurn();
                    break;
                case (nameof(Round.River)):
                    this.SetRiver();
                    break;
            }
        }
        private void Pot_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(Pot.Money):
                    this.PotMoney = _game.CurrentRound.Pot.Money;
                    break;

                case nameof(Pot.ToCall):
                    this.ToCall = _game.CurrentRound.Pot.ToCall;
                    break;
            }
        }

        private void SetRiver()
        {
            this.River = this._game.CurrentRound.River;
            this.PathRiver = Helper.GetCardPath(this.River);
        }

        private void SetTurn()
        {
            this.Turn = this._game.CurrentRound.Turn;
            this.PathTurn = Helper.GetCardPath(this.Turn);
        }

        private void SetFlop()
        {
            if (this._game?.CurrentRound?.Flop.Count == 1)
            {
                this.Flop1 = this._game.CurrentRound.Flop[0];
                this.PathFlop1 = Helper.GetCardPath(this.Flop1);
            }
            if (this._game?.CurrentRound?.Flop.Count == 2)
            {
                this.Flop2 = this._game.CurrentRound.Flop[1];
                this.PathFlop2 = Helper.GetCardPath(this.Flop2);
            }
            if (this._game?.CurrentRound?.Flop.Count == 3)
            {
                this.Flop3 = this._game.CurrentRound.Flop[2];
                this.PathFlop3 = Helper.GetCardPath(this.Flop3);
            }
        }

        private Card _flop1;
        public Card Flop1
        {
            get { return _flop1; }
            set { _flop1 = value; }
        }
        private Card _flop2;
        public Card Flop2
        {
            get { return _flop2; }
            set { _flop2 = value; }
        }
        private Card _flop3;
        public Card Flop3
        {
            get { return _flop3; }
            set { _flop3 = value; }
        }
       
        private Card _turn;
        public Card Turn
        {
            get { return _turn; }
            set { _turn = value; RaisePropertyChangedEvent(); }
        }

        private Card _river;
        public Card River
        {
            get { return _river; }
            set { _river = value; RaisePropertyChangedEvent(); }
        }
        
        private string _pathFlop1;
        public string PathFlop1
        {
            get { return _pathFlop1; }
            set { _pathFlop1 = value; RaisePropertyChangedEvent(); }
        }
        private string _pathFlop2;
        public string PathFlop2
        {
            get { return _pathFlop2; }
            set { _pathFlop2 = value; RaisePropertyChangedEvent(); }
        }
        private string _pathFlop3;
        public string PathFlop3
        {
            get { return _pathFlop3; }
            set { _pathFlop3 = value; RaisePropertyChangedEvent(); }
        }

        private string _pathTurn;
        public string PathTurn
        {
            get { return _pathTurn; }
            set { _pathTurn = value; RaisePropertyChangedEvent(); }
        }

        private string _pathRiver;
        public string PathRiver
        {
            get { return _pathRiver; }
            set { _pathRiver = value; RaisePropertyChangedEvent(); }
        }

        private int _potMoney;
        public int PotMoney
        {
            get { return _potMoney; }
            set { _potMoney = value; RaisePropertyChangedEvent(); }
        }

        private int _forePod;
        public int ForePod
        {
            get { return _forePod; }
            set { _forePod = value; RaisePropertyChangedEvent(); }
        }
        private int _toCall;

        //ToDo Bug im Binding 
        public int ToCall
        {
            get { return _toCall; }
            set
            {
                _toCall = value;
                RaisePropertyChangedEvent();                   
            }
        }
    
        private void NewRound()
        {
            if(_game.CurrentRound != null)
            {
                foreach (var c in _game?.CurrentRound?.GivenCards)
                    c.OnCardIsShown -= Card_OnCardIsShowen;
            }
            
            this._game.StartNewRound();
            
            this._game.CurrentRound.GiveCards();
            this._game.CurrentRound.SetBlinds();

            foreach (var c in _game.CurrentRound.GivenCards)
            {
                c.OnCardIsShown += Card_OnCardIsShowen;
            }
            
            this._game.CurrentRound.HideAll();
        }


        private void Card_OnCardIsShowen(CardEventArgs e)
        {
            if(e.Card.Typ == CardTyp.Flop)
            {
                if (this._game.CurrentRound.Flop[0].IsShown)
                    FlopVisible = "Visible";
                else
                    FlopVisible = "Hidden";
            }
            else if(e.Card.Typ == CardTyp.Turn)
            {
                if (e.Card.IsShown)
                    TurnVisible = "Visible";
                else
                    TurnVisible = "Hidden";
            }
            else if (e.Card.Typ == CardTyp.River)
            {
                if (e.Card.IsShown)
                    RiverVisible = "Visible";
                else
                    RiverVisible = "Hidden";

            }
        }
        
        private string _flopVisible = "Hidden";

        public string FlopVisible
        {
            get { return _flopVisible; }
            set { _flopVisible = value; RaisePropertyChangedEvent(); }
        }

        private string _turnVisible = "Hidden";

        public string TurnVisible
        {
            get { return _turnVisible; }
            set { _turnVisible = value; RaisePropertyChangedEvent(); }
        }

        private string _riverVisible = "Hidden";

        public string RiverVisible
        {
            get { return _riverVisible; }
            set { _riverVisible = value; RaisePropertyChangedEvent(); }
        }


        #region Commands
        public ICommand ButtonNewRound
        {
            get { return new DelegateCommand(NewRound); }
        }



        #endregion
    }
}
