﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace Poker_ProtoType
{
    public class ViewModelPlayer : ObservableObject
    {
        public ViewModelPlayer(Player player)
        {
            this.Player = player;
            this.Player.PropertyChanged -= Player_PropertyChanged;
            this.Player.OnCardAdded -= Player_OnCardAdded;

            this.Player.PropertyChanged += Player_PropertyChanged;
            this.Player.OnCardAdded += Player_OnCardAdded;
        }

        private void Player_OnCardAdded(PlayerEventArgs e)
        {
            if (e.Player.Cards.Count > 1)
                this.PathCard2 = Helper.GetCardPath(e.Player.Cards[1]);
            else
                this.PathCard1 = Helper.GetCardPath(e.Player.Cards[0]);


        }
        
        private void Player_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(Poker_ProtoType.Player.Active):
                    SetActive();
                    break;

                case nameof(Poker_ProtoType.Player.AllowedToCall):
                    this.AllowedToCall = this.Player.AllowedToCall;
                    break;

                case nameof(Poker_ProtoType.Player.AllowedToCheck):
                    this.AllowedToCheck = this.Player.AllowedToCheck;
                    break;

                case nameof(Poker_ProtoType.Player.AllowedToRaise):
                    this.AllowedToRaise = this.Player.AllowedToRaise;
                    break;

                case nameof(Poker_ProtoType.Player.AllowedToFold):
                    this.AllowedToFold = this.Player.AllowedToFold;
                    break;

                case nameof(Poker_ProtoType.Player.IsBigBlind):
                    this.BigBlind = this.Player.IsBigBlind;
                    break;

                case nameof(Poker_ProtoType.Player.Cards):
                    this.Cards = this.Player.Cards;                    
                    break;

                case nameof(Poker_ProtoType.Player.Id):
                    this.Id = this.Player.Id;
                    break;

                case nameof(Poker_ProtoType.Player.Name):
                    this.Name = this.Player.Name;
                    break;

                case nameof(Poker_ProtoType.Player.PlayerAction):
                    this.PlayerAction = this.Player.PlayerAction;
                    break;

                case nameof(Poker_ProtoType.Player.SetMoney):
                    this.SetMoney = this.Player.SetMoney;
                    break;

                case nameof(Poker_ProtoType.Player.IsSmallBlind):
                    this.SmallBlind = this.Player.IsSmallBlind;
                    break;

                case nameof(Poker_ProtoType.Player.Stack):
                    this.Stack = this.Player.Stack;
                    break;

                case nameof(Poker_ProtoType.Player.RotationCount):
                    this.RotationCount = this.Player.RotationCount;
                    break;

                default:
                    break;
            }
        }

        private void SetActive()
        {
            this.Active = this.Player.Active;
        }

        private Player player;
        
        #region Fields      
        private int _id;
        private string _name;

        private int _stack;
        private int _setMoney;

        private List<Card> _cards;
        private PlayerAction _playerAction = PlayerAction.None;

        private bool _allowedToCheck = false;
        private bool _allowedToRaise = false;
        private bool _allowedToCall = false;
        private bool _allowedToFold = false;

        private bool _active = false;

        private bool _bigBlind = false;
        private bool _smallBlind = false;
        private string _role;

        private string _pathCard1;
        private string _pathCard2;

        private int _rotationCount;

        private Brush _bgColor;

        #endregion

        #region Properties
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                this.RaisePropertyChangedEvent();
            }
        }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                this.RaisePropertyChangedEvent();
            }
        }

        public int Stack
        {
            get
            {
                return _stack;
            }
            set
            {
                _stack = value;
                this.RaisePropertyChangedEvent();
            }
        }
        public int SetMoney
        {
            get
            {
                return _setMoney;
            }
            set
            {
                _setMoney = value;
                this.RaisePropertyChangedEvent();
            }
        }

        public List<Card> Cards
        {
            get
            {
                return _cards;
            }
            set
            {
                _cards = value;
                this.RaisePropertyChangedEvent();
            }
        }
        public PlayerAction PlayerAction
        {
            get
            {
                return _playerAction;
            }
            set
            {
                _playerAction = value;
                if (_playerAction == PlayerAction.None)
                    _playerAction = PlayerAction.Activated;

                this.RaisePropertyChangedEvent();
            }
        }

        public bool AllowedToCheck
        {
            get
            {
                return _allowedToCheck;
            }
            set
            {
                _allowedToCheck = value;
                this.RaisePropertyChangedEvent();
            }
        }
        public bool AllowedToRaise
        {
            get
            {
                return _allowedToRaise;
            }

            set
            {
                _allowedToRaise = value;
                this.RaisePropertyChangedEvent();
            }
        }
        public bool AllowedToCall
        {
            get
            {
                return _allowedToCall;
            }
            set
            {
                _allowedToCall = value;
                this.RaisePropertyChangedEvent();
            }
        }
        public bool AllowedToFold
        {
            get
            {
                return _allowedToFold;
            }

            set
            {
                _allowedToFold = value;
                RaisePropertyChangedEvent();
            }
        }

        public bool BigBlind
        {
            get
            {
                return _bigBlind;
            }
            set
            {
                _bigBlind = value;
                this.RaisePropertyChangedEvent();
                if (_bigBlind) this.Role = "Big Blind";
                if (!_smallBlind && !_bigBlind) this.Role = "";
            }
        }
        public bool SmallBlind
        {
            get
            {
                return _smallBlind;
            }
            set
            {
                _smallBlind = value;
                this.RaisePropertyChangedEvent();
                if (_smallBlind) this.Role = "Small Blind";
                if (!_smallBlind && !_bigBlind) this.Role = "";
            }
        }

        public bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
                this.RaisePropertyChangedEvent();
                ChangeSomeFuckingColor(_active);
                this.PlayerAction = player.PlayerAction;
            }
        }

        public Player Player
        {
            get
            {
                return player;
            }

            set
            {
                player = value;
            }
        }

        public string Role
        {
            get
            {
                return _role;
            }
            set
            {
                _role = value;
                RaisePropertyChangedEvent();
            }
        }

        public string PathCard1
        {
            get { return _pathCard1; }
            set { _pathCard1 = value; RaisePropertyChangedEvent(); }
        }
        public string PathCard2
        {
            get { return _pathCard2; }
            set { _pathCard2 = value; RaisePropertyChangedEvent(); }
        }

        public int RotationCount
        {
            get
            {
                return _rotationCount;
            }

            set
            {
                _rotationCount = value;
                RaisePropertyChangedEvent();
            }
        }

        public Brush BgColor
        {
            get
            {
                return _bgColor;
            }

            set
            {
                _bgColor = value;
                RaisePropertyChangedEvent();
            }
        }
        #endregion

        private void ChangeSomeFuckingColor(bool active)
        {

            if (!active)
            {
                BgColor = new SolidColorBrush(Color.FromArgb(40, 10, 20, 15));

            }
            else
            {
                BgColor = new SolidColorBrush(Color.FromArgb(100, 100, 0, 0));
            }
        }



        #region Button Commands
        public ICommand ButtonFold
        {
            get { return new DelegateCommand(Fold); }
        }
        private void Fold()
        {
            this.Player.Fold();
        }

        public ICommand ButtonCheck
        {
            get { return new DelegateCommand(Check); }
        }
        private void Check()
        {
            this.Player.Check();
        }

        public ICommand ButtonRaise
        {
            get { return new DelegateCommand(Raise); }
        }
        private void Raise()
        {
            Random rnd = new Random(DateTime.Now.Millisecond);
            this.Player.Raise(rnd.Next(1,99));
        }

        public ICommand ButtonCall
        {
            get { return new DelegateCommand(Call); }
        }

        private void Call()
        {
            this.Player.Call();
        }
        #endregion
    }
}

