﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Poker_ProtoType
{
    /// <summary>
    /// Interaction logic for Table.xaml
    /// </summary>
    public partial class Table : Window
    {
        private byte nextOne = 0;

        public Table()
        {
            InitializeComponent();
            var vmm = new ViewModelMain();
            this.DataContext = vmm;

            for (int i = 0; i < vmm.ViewModelPlayer.Count; i++)
            {
                var uc = new UIPlayer(vmm.ViewModelPlayer[i]);

                nextOne += 30;

                System.Windows.Media.Brush b = new SolidColorBrush(Color.FromArgb(40, 10, nextOne, nextOne));
                System.Windows.Media.Brush b2 = new SolidColorBrush(Color.FromArgb(100, 100, 0, 0));
                uc.Background = b;

                if (i == 0)
                    colP1.Children.Add(uc);
                if (i == 1)
                    colP2.Children.Add(uc);
                if (i == 2)
                    colP3.Children.Add(uc);
                if (i == 3)
                    colP4.Children.Add(uc);

                if (vmm.ViewModelPlayer[i].Id == 1)
                {
                    uc.Background = b;
                }
            }
        }
    }
}
