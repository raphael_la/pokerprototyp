﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Poker_ProtoType
{
    /// <summary>
    /// Interaction logic for UIPlayer.xaml
    /// </summary>
    public partial class UIPlayer : UserControl
    {
        public UIPlayer(ViewModelPlayer viewModelPlayer)
        {
            InitializeComponent();
            this.VMPlayer = viewModelPlayer;
            this.DataContext = VMPlayer;
        }

        public UIPlayer() { }

        private ViewModelPlayer _viewModelPlayer;
        public ViewModelPlayer VMPlayer
        {
            get { return _viewModelPlayer; }
            set { _viewModelPlayer = value; }
        }
    }
}
